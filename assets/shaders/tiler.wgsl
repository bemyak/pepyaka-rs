#import bevy_sprite::mesh2d_bindings      mesh
#import bevy_sprite::mesh2d_functions     mesh2d_position_local_to_clip

@group(1) @binding(0)
var texture: texture_2d<f32>;
@group(1) @binding(1)
var my_sampler: sampler;
@group(1) @binding(2)
var<uniform> input: Input;
struct Input {
    rect: vec2<f32>,
    offset: vec2<f32>,
};
struct Vertex {
    @location(0) position: vec3<f32>,
    @location(1) uv: vec2<f32>,
};

struct VertexOutput {
    @builtin(position) position: vec4<f32>,
    @location(1) uv: vec2<f32>,
}

@vertex
fn vertex(vertex: Vertex) -> VertexOutput {
    let texture_size = vec2f(textureDimensions(texture));
    let scale = texture_size / input.rect;
    let stretched = vec2f(vertex.position.x, vertex.position.y * scale.y - input.offset.y);
    var out: VertexOutput;
    out.position = mesh2d_position_local_to_clip(
        mesh.model,
        vec4<f32>(stretched, vertex.position.z, 1.0)
    );
    out.uv = vec2f(vertex.uv.x / scale.x - input.offset.x / input.rect.x, vertex.uv.y);
    return out;
}

@fragment
fn fragment(
    mesh: VertexOutput,
) -> @location(0) vec4<f32> {
    return textureSample(texture, my_sampler, fract(mesh.uv));
}
