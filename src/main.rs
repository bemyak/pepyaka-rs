#![allow(unused_imports)]
mod camera;
mod camera_target;
mod clouds;
mod full_screen_rect;
// mod game;
mod outside;
mod parallax;
mod player;
mod scene;
mod tiler;

use std::time::Duration;

use bevy::{
	asset::{diagnostic::AssetCountDiagnosticsPlugin, ChangeWatcher},
	diagnostic::{EntityCountDiagnosticsPlugin, FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin},
	ecs::schedule::ScheduleLabel,
	prelude::*,
	render::{
		settings::{Backends, PowerPreference, WgpuSettings},
		RenderPlugin,
	},
	window::{PresentMode, WindowResolution},
};
use bevy_editor_pls::prelude::*;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use {camera::CharacterCameraPlugin, outside::Outside, parallax::ParallaxPlugin, scene::Scene};

fn main() {
	App::new()
		// Bevy plugins
		.add_plugins(
			DefaultPlugins
				.set(AssetPlugin {
					watch_for_changes: ChangeWatcher::with_delay(Duration::from_millis(200)),
					..Default::default()
				})
				.set(
					// Use integrated graphics card
					RenderPlugin {
						wgpu_settings: WgpuSettings {
							power_preference: PowerPreference::LowPower,
							backends: Some(Backends::VULKAN),
							..default()
						},
					},
				)
				// Set default window ratio to 4:3
				.set(WindowPlugin {
					primary_window: Some(Window {
						title: "Hungry Pupu".to_string(),
						resolution: WindowResolution::new(1280.0, 960.0),
						// present_mode: PresentMode::AutoNoVsync,
						..Default::default()
					}),
					..Default::default()
				}),
		)
		// 3rd-party plugins
		// .add_plugins(WorldInspectorPlugin::new())
		// .add_plugins(AssetCountDiagnosticsPlugin::default())
		.add_plugins(EditorPlugin::default())
		.add_plugins(FrameTimeDiagnosticsPlugin::default())
		.add_plugins(EntityCountDiagnosticsPlugin::default())
		.add_plugins(LogDiagnosticsPlugin::default())
		// States
		.add_state::<GameState>()
		.add_state::<Scene>()
		// My plugins
		.add_plugins(CharacterCameraPlugin)
		.add_plugins(ParallaxPlugin)
		.add_plugins(Outside)
		.run();
}

#[derive(States, Debug, Clone, Eq, PartialEq, Hash, Default)]
pub enum GameState {
	MainMenu,
	#[default]
	Game,
	Paused,
}
