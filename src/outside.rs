use bevy::{
	pbr::{MaterialPipeline, MaterialPipelineKey},
	prelude::*,
	reflect::{TypePath, TypeUuid},
	render::{
		camera::{camera_system, Viewport},
		mesh::MeshVertexBufferLayout,
		render_resource::{
			AsBindGroup, RenderPipelineDescriptor, ShaderRef, ShaderType,
			SpecializedMeshPipelineError,
		},
		view::WindowSystem,
	},
	sprite::{Material2d, Material2dKey, Material2dPlugin, MaterialMesh2dBundle, Mesh2dHandle},
	window::WindowResized,
};

use crate::{
	camera::CharacterCamera,
	camera_target::{target_follows_player, CameraTargetBundle},
	clouds::generate_clouds,
	full_screen_rect::{
		update_full_screen_rect, update_tiling_rect, FullScreenMesh, FullScreenRectBundle,
	},
	parallax::Parallax,
	player::{move_player, Player},
	scene::{Scene, SceneBundle, SceneSize},
	tiler::{SpriteTiling, TilerInput, TilingMaterial},
	GameState,
};

#[derive(Component, Debug, Default)]
pub struct Outside;

impl Plugin for Outside {
	fn build(&self, app: &mut App) {
		app.add_plugins(Material2dPlugin::<TilingMaterial>::default())
			.add_systems(OnEnter(Scene::Outside), spawn)
			.add_systems(Update, move_player.run_if(in_state(Scene::Outside)))
			.add_systems(Update, target_follows_player)
			// TODO: Make these one-time?
			.add_systems(Update, (update_full_screen_rect, update_tiling_rect))
			.add_systems(OnExit(Scene::Outside), despawn);
	}
}

fn spawn(
	mut commands: Commands,
	assets: Res<AssetServer>,
	mut meshes: ResMut<Assets<Mesh>>,
	mut materials: ResMut<Assets<TilingMaterial>>,
	// projection: Query<&OrthographicProjection, With<CharacterCamera>>,
) {
	// let projection = projection.get_single().unwrap();
	// let rect = projection.area.max - projection.area.min;
	commands
		.spawn(SceneBundle {
			scene: Outside,
			scene_size: SceneSize { x: 10_000. },
			spatial: SpatialBundle::default(),
		})
		.with_children(|parent| {
			parent
				.spawn(SpriteBundle {
					texture: assets.load("lvl1/00_sky.png"),
					transform: Transform::from_xyz(0.0, 0.0, -100.0),
					..Default::default()
				})
				.insert(Parallax::from(0.0))
				.insert(Name::new(format!("Sky")));

			let clouds = generate_clouds(Res::clone(&assets), 10_000.);
			for (i, cloud) in clouds.into_iter().enumerate() {
				parent
					.spawn(cloud)
					.insert(Parallax::from(0.05))
					.insert(Name::new(format!("Cloud{i}")));
			}

			parent
				.spawn(FullScreenRectBundle {
					material: materials.add(TilingMaterial {
						texture: assets.load("lvl1/02_hill1_tile.png"),
						settings: Default::default(),
					}),
					mesh: meshes.add(shape::Quad::new(Vec2::ZERO).into()).into(),
					transform: Transform::from_xyz(0.0, 0.0, 2.0),
					..Default::default()
				})
				.insert(Parallax::from(0.01))
				.insert(Name::new(format!("Hill02")));
			parent
				.spawn(FullScreenRectBundle {
					material: materials.add(TilingMaterial {
						texture: assets.load("lvl1/03_hill1_tile.png"),
						settings: Default::default(),
					}),
					mesh: meshes.add(shape::Quad::new(Vec2::ZERO).into()).into(),
					transform: Transform::from_xyz(0.0, 0.0, 3.0),
					..Default::default()
				})
				.insert(Parallax::from(0.1))
				.insert(Name::new(format!("Hill03")));
			parent
				.spawn(SpriteBundle {
					texture: assets.load("pepyaka/pepyaka_idle.png"),
					..Default::default()
				})
				.insert(Player)
				.insert(Name::new("Pepyaka"));
			parent.spawn(CameraTargetBundle {
				..Default::default()
			})
			.insert(Name::new(format!("CameraTarget")));
		});
}

fn despawn(mut commands: Commands, query: Query<Entity, With<Outside>>) {
	for entity in query.iter() {
		commands.entity(entity).despawn_recursive();
	}
}
