use bevy::{
	prelude::*,
	sprite::{Material2d, Mesh2dHandle},
};

use crate::{camera::CharacterCamera, tiler::TilingMaterial};

/// A component bundle for entities with a [`Mesh2dHandle`] and a [`Material2d`].
#[derive(Bundle, Clone, Debug, Default)]
pub struct FullScreenRectBundle<M: Material2d> {
	pub mesh: Mesh2dHandle,
	pub material: Handle<M>,
	pub marker: FullScreenMesh,
	pub transform: Transform,
	pub global_transform: GlobalTransform,
	/// User indication of whether an entity is visible
	pub visibility: Visibility,
	/// Algorithmically-computed indication of whether an entity is visible and should be extracted for rendering
	pub computed_visibility: ComputedVisibility,
}

#[derive(Component, Debug, Default, Clone)]
pub struct FullScreenMesh;

pub fn update_full_screen_rect(
	projection: Query<&OrthographicProjection, With<CharacterCamera>>,
	mut mesh: Query<(&Mesh2dHandle, &mut Transform, With<FullScreenMesh>)>,
	mut meshes: ResMut<Assets<Mesh>>,
) {
	let projection = projection.get_single().unwrap();
	let rect = projection.area.max - projection.area.min;
	let origin = projection.area.min + rect / 2.;
	for (mesh, mut transform, ..) in mesh.iter_mut() {
		transform.translation = origin.extend(transform.translation.z);
		let mesh = meshes.get_mut(&mesh.0).unwrap();
		*mesh = shape::Quad::new(rect).into();
	}
}

pub fn update_tiling_rect(
	projection: Query<&OrthographicProjection, With<CharacterCamera>>,
	material: Query<&Handle<TilingMaterial>, With<FullScreenMesh>>,
	mut materials: ResMut<Assets<TilingMaterial>>,
) {
	let projection = projection.get_single().unwrap();
	let rect = projection.area.max - projection.area.min;

	for handle in material.iter() {
		let m = materials.get_mut(handle).unwrap();
		m.settings.rect = rect;
	}
}
