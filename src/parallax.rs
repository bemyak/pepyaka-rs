use bevy::{prelude::*, sprite::Mesh2dHandle, transform::TransformSystem};

use crate::{
	camera::{CameraMoved, CharacterCamera},
	camera_target::{camera_follows_target, CameraTarget},
	tiler::TilingMaterial,
	GameState,
};

pub struct ParallaxPlugin;

impl Plugin for ParallaxPlugin {
	fn build(&self, app: &mut App) {
		app.add_systems(
			PostUpdate,
			(move_parallax, move_parallax_tiling)
				.after(camera_follows_target)
				.before(TransformSystem::TransformPropagate)
				.run_if(in_state(GameState::Game)),
		);
	}
}

#[derive(Component, Default, Debug)]
pub struct Parallax {
	pub scale: Vec2,
}

impl From<f32> for Parallax {
	fn from(value: f32) -> Self {
		Self {
			scale: Vec2::new(value, value),
		}
	}
}

fn move_parallax(
	mut camera_moved_event: EventReader<CameraMoved>,
	mut layer_query: Query<(&mut Transform, &Parallax, Without<Handle<TilingMaterial>>)>,
) {
	let mut d = Vec3::default();
	for camera_moved in camera_moved_event.iter() {
		d += camera_moved.0
	}
	for (mut transform, parallax, ..) in layer_query.iter_mut() {
		transform.translation += d * (Vec3::ONE - parallax.scale.extend(0.));
	}
}

fn move_parallax_tiling(
	mut materials: ResMut<Assets<TilingMaterial>>,
	mut camera_moved_event: EventReader<CameraMoved>,
	mut tilings: Query<(&Handle<TilingMaterial>, &mut Transform, &Parallax)>,
) {
	let mut d = Vec2::default();
	for camera_moved in camera_moved_event.iter() {
		d += camera_moved.0.truncate();
	}
	for (material, mut transform, parallax, ..) in tilings.iter_mut() {
		let m = materials.get_mut(material).unwrap();
		m.settings.offset.x += d.x * parallax.scale.x;
		transform.translation.y += d.y * parallax.scale.y;
	}
}
