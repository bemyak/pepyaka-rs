use bevy::{
	core_pipeline::tonemapping::Tonemapping, prelude::*, render::camera::ScalingMode,
	transform::TransformSystem,
};

use crate::{camera_target::camera_follows_target, GameState};

pub struct CharacterCameraPlugin;

impl Plugin for CharacterCameraPlugin {
	fn build(&self, app: &mut App) {
		app.add_event::<CameraMoved>()
			.add_systems(Startup, spawn_camera)
			.add_systems(
				PostUpdate,
				camera_follows_target
					.before(TransformSystem::TransformPropagate)
					.run_if(in_state(GameState::Game)),
			)
			.add_systems(OnExit(GameState::Game), despawn);
	}
}

#[derive(Component)]
pub struct CharacterCamera;

#[derive(Event, Debug, Default)]
pub struct CameraMoved(pub Vec3);

pub fn spawn_camera(mut commands: Commands) {
	commands
		.spawn(Camera2dBundle {
			camera: Camera {
				hdr: true, // 1. HDR is required for bloom
				..default()
			},
			tonemapping: Tonemapping::TonyMcMapface, // 2. Using a tonemapper that desaturates to white is recommended
			projection: OrthographicProjection {
				far: 1000.,
				near: -1000.,
				scaling_mode: ScalingMode::FixedHorizontal(3840.0),
				..Default::default()
			},
			..Default::default()
		})
		.insert(CharacterCamera);
}

pub fn despawn(mut commands: Commands, camera_query: Query<Entity, With<CharacterCamera>>) {
	// let c = camera_query.get_single_mut().unwrap();
	for camera in camera_query.iter() {
		commands.entity(camera).despawn()
	}
}
