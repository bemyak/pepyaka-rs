use bevy::{
	core_pipeline::tonemapping::Tonemapping, prelude::*, render::camera::ScalingMode,
	transform::TransformSystem,
};

use crate::{
	camera::{CameraMoved, CharacterCamera},
	player::Player,
};

#[derive(Component, Debug, Default)]
pub struct CameraTarget;

#[derive(Bundle, Debug, Default)]
pub struct CameraTargetBundle {
	pub spatial: SpatialBundle,
}

pub fn camera_follows_target(
	mut camera_query: Query<(&mut Transform, With<CharacterCamera>, Without<Player>)>,
	target_query: Query<(&Transform, With<Player>)>,
	mut camera_moved_event: EventWriter<CameraMoved>,
) {
	let (mut camera_pos, ..) = camera_query.get_single_mut().unwrap();
	let Ok((target, ..)) = target_query.get_single() else {
		return;
	};
	// TODO: lerp
	let d = Vec3 {
		x: target.translation.x - camera_pos.translation.x,
		y: target.translation.y - camera_pos.translation.y,
		z: 0.0,
	};
	camera_pos.translation += d;
	camera_moved_event.send(CameraMoved(d));
}

pub fn target_follows_player(
	player_query: Query<&GlobalTransform, With<Player>>,
	mut camera_target_query: Query<&mut Transform, With<CameraTarget>>,
) {
	let Ok(global_transform) = player_query.get_single() else {
		return;
	};
	let Ok(mut camera_target) = camera_target_query.get_single_mut() else {
		return;
	};
	camera_target.translation.x = global_transform.translation().x;
	camera_target.translation.y = global_transform.translation().y;
}
