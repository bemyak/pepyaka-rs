use bevy::{
	prelude::*,
	reflect::{TypePath, TypeUuid},
	render::{
		mesh::MeshVertexBufferLayout,
		render_resource::{
			AsBindGroup, RenderPipelineDescriptor, ShaderRef, ShaderType,
			SpecializedMeshPipelineError,
		},
	},
	sprite::{Material2d, Material2dKey},
};

use crate::{parallax::Parallax, scene::SceneSize};

#[derive(Debug, Clone, Default, ShaderType)]
pub struct TilerInput {
	pub rect: Vec2,
	pub offset: Vec2,
}

#[derive(AsBindGroup, TypeUuid, TypePath, Debug, Clone, Default)]
#[uuid = "55c76d78-2497-4726-9f83-25c6c0b4b919"]
pub struct TilingMaterial {
	#[texture(0, dimension = "2d")]
	#[sampler(1)]
	pub texture: Handle<Image>,
	#[uniform(2)]
	pub settings: TilerInput,
}

impl Material2d for TilingMaterial {
	fn vertex_shader() -> ShaderRef {
		"shaders/tiler.wgsl".into()
	}
	fn fragment_shader() -> ShaderRef {
		"shaders/tiler.wgsl".into()
	}
	fn specialize(
		descriptor: &mut RenderPipelineDescriptor,
		layout: &MeshVertexBufferLayout,
		_key: Material2dKey<Self>,
	) -> Result<(), SpecializedMeshPipelineError> {
		let vertex_layout = layout.get_layout(&[
			Mesh::ATTRIBUTE_POSITION.at_shader_location(0),
			Mesh::ATTRIBUTE_UV_0.at_shader_location(1),
		])?;
		descriptor.vertex.buffers = vec![vertex_layout];
		Ok(())
	}
}

#[derive(Component, Debug, Default)]
pub struct SpriteTiling {
	pub offset: Vec2,
}

pub fn _spawn_tiled(
	mut commands: Commands,
	images: Res<Assets<Image>>,
	level_size_query: Query<&SceneSize>,
	sprite_query: Query<(
		&Transform,
		&Handle<Image>,
		Option<&Parallax>,
		&SpriteTiling,
		Option<&Parent>,
	)>,
) {
	let lvl_size = level_size_query.get_single().unwrap();
	for (position, texture, parallax, tiling_settings, parent) in sprite_query.iter() {
		let img = images.get(texture).unwrap();
		let mut last_pos = position.translation.truncate();
		info!("last_pos={last_pos}");
		let limit = lvl_size.x * parallax.map(|p| p.scale.x).unwrap_or(1.0);
		while last_pos.x < lvl_size.x * limit {
			let new_pos = last_pos + Vec2::new(img.size().x, 0.) + tiling_settings.offset;
			let bundle = SpriteBundle {
				texture: texture.clone(),
				transform: Transform::from_translation(new_pos.extend(position.translation.z)),
				..Default::default()
			};

			let spawned = commands.spawn(bundle).id();
			if let Some(parent) = parent {
				commands.entity(**parent).push_children(&[spawned]);
			}

			last_pos = new_pos;
		}
	}
}
