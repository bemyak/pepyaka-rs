use bevy::prelude::*;
use rand::Rng;

use crate::scene::SceneSize;

#[derive(Component, Debug, Default)]
pub struct Cloud;

#[derive(Bundle, Default)]
pub struct CloudBundle {
	pub marker: Cloud,
	pub sprite: SpriteBundle,
}

const ASSETS: [&'static str; 2] = [
	"lvl1/01_cloud1.webp",
	"lvl1/01_cloud2.webp",
	// "lvl1/01_cloud3.webp",
];

impl CloudBundle {
	fn new_random(assets: Res<AssetServer>, x: f32, top_y: f32, z: f32) -> Self {
		let mut rng = rand::thread_rng();
		let i = rng.gen_range(0..ASSETS.len());
		let asset: Handle<Image> = assets.load(ASSETS[i]);
		let y = if i == 3 {
			top_y
		} else {
			let offset: f32 = rng.gen::<f32>() * 1000.;
			top_y + offset
		};
		CloudBundle {
			marker: Cloud,
			sprite: SpriteBundle {
				transform: Transform::from_translation(Vec3::new(x, y, z)),
				texture: asset,
				sprite: Sprite {
					color: Color::rgba(1.0, 1.0, 1.0, 0.7),
					..Default::default()
				},
				..Default::default()
			},
		}
	}
}

pub fn generate_clouds(assets: Res<AssetServer>, level_size: f32) -> Vec<CloudBundle> {
	let y = 1000.;
	let z = -99.0;
	let mut rng = rand::thread_rng();
	let level_size = level_size.ceil() as i32;
	(0..level_size)
		.step_by(666)
		.map(|x| {
			let offset = rng.gen_range(-100..=100);
			let x = x + offset;
			CloudBundle::new_random(Res::clone(&assets), x as f32, y, z)
		})
		.collect()
}
