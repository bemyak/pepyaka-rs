//! Common utils for levels

use bevy::prelude::*;
use std::fmt::Debug;

#[derive(States, Component, Debug, Clone, Eq, PartialEq, Hash, Default)]
pub enum Scene {
	#[default]
	Outside,
}

#[derive(Component, Default, Debug)]
pub struct SceneSize {
	pub x: f32,
}

#[derive(Bundle, Debug, Default)]
pub struct SceneBundle<S: Component + Plugin + Debug + Default> {
	pub scene: S,
	pub scene_size: SceneSize,
	pub spatial: SpatialBundle,
}
